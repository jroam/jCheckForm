## jCheckForm表单验证的jquery插件!!2
### jroam开发的一个表单验证jquery插件,一行搞定验证

需要验证的input必须设置datatype值   
 errormsg 验证不通过时显示的信息  
 errormsg-bj 比较验证失败后显示的提示信息
 idtypes 目前值可以为:ueditor  表示这次验证的是百度编辑器的内容  没有开发

内置类型 int * str float email tel phone  url cn (中文) zh(中文) 还可以是正则表达式

用法例子: 如:
	  int[6-10] 表示验证6至10之间的数字
	  int[5-] 大于等于5  int[-10]小于等于10的数
	  int[-3--1] 验证-3到-1之间的数
	  float[-20] 小于等于20的浮点数
	  str[4-8]4到8位的字符串
	  cn[3-6] 表示3到6个中文字符
	  zh[3-6] 表示3到6个中文字符
	  /[a-z]{3,}/   表示3个及以上的a至z之间的数

		


循环检查，如果第一个不通过就返回false 全部通过返回true

使用前请自行载入jquery库

使用实例1:
```
<form id="#form_add">
<input type="text" datatype="int[1-100]" errormsg="请填写1至100以内的数" name="v1">
</form>
```


比较关系(比较和id为passd1的值):eq的值将做为jq选择器传入:
```
<input type="text" eq="#passd1" datatype="str[5-20]" errormsg-bj="密码两次不相同,关且密码长度在5至20位之间" name="passd2">

```
用法示例:

```
$("#btn_add").click(function(){
			if($("#form_add").jcheckform()) $("#form_add").submit();//检查通过就提交表单
			return false;
		});
```

